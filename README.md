# Architecture Diagram

![alt architecture diagram](architecture-diagram.png)


### Requirements

- [Install the Serverless Framework](https://serverless.com/framework/docs/providers/aws/guide/installation/)
- [Configure your AWS CLI](https://serverless.com/framework/docs/providers/aws/guide/credentials/)

### Usage

To fetch dependencies

``` bash
$ npm install
```

To run unit test

``` bash
$ npm run test
```

To run a function on your local

``` bash
$ serverless invoke local --function <func_name> --path <path/to/mock/data>
```

Deploy your project

``` bash
$ serverless deploy
```

Deploy a single function

``` bash
$ serverless deploy function --function <func_name>
```