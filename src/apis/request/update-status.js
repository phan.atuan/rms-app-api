import moment from 'moment';
import { call } from '../../libs/dynamodb-lib';
import { invoke } from '../../libs/lambda-lib';
import { success, failure } from '../../libs/response-lib';
import validate from '../../libs/validation-lib';
import { request2entity } from '../../libs/transformation-lib';

export function main(event, context, callback) {
  const data = JSON.parse(event.body);
  const { cognitoIdentityId } = event.requestContext.identity;
  const requestId = event.pathParameters.id;
  const {
    text,
    status,
    fulfilmentDate,
    submissionDate,
  } = data;

  // validate input
  const { errors } = validate(data, 'updateStatus');
  if (errors) {
    callback(null, success(Object.assign({}, { status: false, errors })));
    return;
  }

  const comment = {
    body: JSON.stringify({
      text,
      requestId,
    }),
    requestContext: {
      identity: {
        cognitoIdentityId,
      },
    },
  };

  invoke({ // call lambda function to create new comment
    FunctionName: process.env.LAMBDA_FUNC_COMMENT_CREATION,
    Payload: JSON.stringify(comment), // pass params
    InvocationType: 'RequestResponse',
  }, async (error) => {
    if (error) {
      console.log(error);
      return callback(null, failure({ status: false }));
    }
    const params = {
      TableName: 'requests',
      // 'Key' defines the partition key and sort key of the item to be updated
      // - 'userId': Identity Pool identity id of the authenticated user
      // - 'id': path parameter
      Key: {
        PK: cognitoIdentityId, // PK = userId
        SK: requestId, // SK= requestId
      },
      // 'UpdateExpression' defines the attributes to be updated
      // 'ExpressionAttributeValues' defines the value in the update expression
      UpdateExpression: `SET Attrib5 = :fulfilmentDate
                          , #LSI1SK = :statusDate
                          , Attrib6 = :status`,
      ExpressionAttributeNames: {
        '#LSI1SK': 'LSI1-SK',
      },
      ExpressionAttributeValues: {
        ':fulfilmentDate': (fulfilmentDate) ? moment(fulfilmentDate, 'YYYY-MM-DD').valueOf() : Date.now(),
        ':statusDate': `${status}#${moment(submissionDate).format('YYYY-MM-DD')}`,
        ':status': status,
      },
      ReturnValues: 'ALL_NEW',
    };

    try {
      const { Attributes } = await call('update', params);
      const item = request2entity(Attributes);
      return callback(null, success(Object.assign({}, { item }, { status: true })));
    } catch (e) {
      console.log(e);
      return callback(null, failure({ status: false }));
    }
  });
}
