import { call } from '../../libs/dynamodb-lib';
import { success, failure } from '../../libs/response-lib';
import { request2entity } from '../../libs/transformation-lib';

export async function main(event, context, callback) {
  // status can be either of Open, Close, Cancel and All
  const { status } = event.pathParameters;
  const decodedToken = event.queryStringParameters.token ? Buffer.from(event.queryStringParameters.token, 'base64').toString() : null;
  const token = JSON.parse(decodedToken);
  const Limit = JSON.parse(event.queryStringParameters.limit);
  // 'KeyConditionExpression' defines the condition for the query
  // - 'userId = :userId': only return items with matching 'userId'
  //   partition key
  // 'ExpressionAttributeValues' defines the value in the condition
  // - ':userId': defines 'userId' to be Identity Pool identity id
  //   of the authenticated user
  let IndexName;
  let KeyConditionExpression;
  let ExpressionAttributeValues;
  let ExpressionAttributeNames = null;

  switch (status) {
    case 'Open':
    case 'Close':
    case 'Cancel':
      IndexName = 'LSI1';
      KeyConditionExpression = 'PK = :userId and begins_with(#LSI1SK,:statusDate)';
      ExpressionAttributeValues = {
        ':userId': event.requestContext.identity.cognitoIdentityId,
        ':statusDate': `${status}`,
      };
      ExpressionAttributeNames = {
        '#LSI1SK': 'LSI1-SK',
      };
      break;
    default:
      IndexName = 'LSI2';
      KeyConditionExpression = 'PK = :userId';
      ExpressionAttributeValues = {
        ':userId': event.requestContext.identity.cognitoIdentityId,
      };
  }
  const params = {
    TableName: 'requests',
    IndexName,
    KeyConditionExpression,
    ExpressionAttributeValues,
    ExpressionAttributeNames,
    ScanIndexForward: false,
    Limit,
  };

  if (token) {
    params.ExclusiveStartKey = token;
  }

  try {
    const result = await call('query', params);
    // Return the matching list of items in response body
    const items = result.Items.map(request2entity);
    callback(null, success(Object.assign(
      {},
      { items },
      { token: (result.LastEvaluatedKey ? Buffer.from(JSON.stringify(result.LastEvaluatedKey)).toString('base64') : null), status: true },
    )));
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false }));
  }
}
