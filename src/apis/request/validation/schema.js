import joi from 'joi';

export default joi.object().keys({
  userId: joi.string().required().error(() => 'userId:User Id is required'),
  requestId: joi.string().required().error(() => 'requestId:Request Id is required'),
  accountName: joi.string().required().error(() => 'accountName:Account Name is required'),
  resourceType: joi.string().required().error(() => 'resourceType:Resource Type is required'),
  resourceRate: joi.number().required().error(() => 'resourceRate:Rate is required'),
  quantity: joi.number().required().error(() => 'quantity:Quantity must be a number'),
  submissionDate: [joi.string().allow(null), joi.string().regex(/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))?$/).error(() => 'submissionDate:Invalid date format')],
  tentativeStartDate: [joi.string().allow(null), joi.string().regex(/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))?$/).error(() => 'tentativeStartDate:Invalid date format')],
  fulfilmentDate: [joi.string().allow(null), joi.string().regex(/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))?$/).error(() => 'fulfilmentDate:Invalid date format')],
  status: joi.string().valid(['Open', 'Close', 'Cancel']).required().error(() => 'status:Status must be in [Open, Close, Cancel]'),
});

