import joi from 'joi';

export default joi.object().keys({
  status: joi.string().valid(['Open', 'Close', 'Cancel']).required().error(() => 'status:Status must be in [Open, Close, Cancel]'),
  submissionDate: joi.string().regex(/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))?$/).error(() => 'submissionDate:Invalid date format'),
  fulfilmentDate: [joi.string().allow(null), joi.string().regex(/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))?$/).error(() => 'fulfilmentDate:Invalid date format')],
  text: joi.string().required().error(() => 'text:Comment is required'),
});
