import { call } from '../../libs/dynamodb-lib';
import { success, failure } from '../../libs/response-lib';
import { request2entity } from '../../libs/transformation-lib';

export async function main(event, context, callback) {
  const params = {
    TableName: 'requests',
    // 'Key' defines the partition key and sort key of the item to be retrieved
    // - 'userId': Identity Pool identity id of the authenticated user
    // - 'requestId': path parameter
    Key: {
      PK: event.requestContext.identity.cognitoIdentityId, // PK = userId
      SK: event.pathParameters.id, // SK = requestId
    },
  };

  try {
    const result = await call('get', params);
    if (result.Item) {
      // Return the retrieved item
      callback(null, success(Object.assign(
        {},
        { item: request2entity(result.Item) },
        { status: true },
      )));
    } else {
      callback(null, success({ status: false, errors: { message: 'Item not found.' } }));
    }
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false }));
  }
}
