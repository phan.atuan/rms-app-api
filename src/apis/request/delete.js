import { call } from '../../libs/dynamodb-lib';
import { success, failure } from '../../libs/response-lib';
import { invoke } from '../../libs/lambda-lib';

export function main(event, context, callback) {
  const { id } = event.pathParameters;
  const payload = {
    pathParameters: {
      id,
    },
  };
  invoke({ // call lambda function to create new comment
    FunctionName: process.env.LAMBDA_FUNC_COMMENT_DELETE,
    Payload: JSON.stringify(payload), // pass params
    InvocationType: 'RequestResponse',
  }, async (error) => {
    if (error) {
      console.log(error);
      return callback(null, failure({ status: false }));
    }
    const params = {
      TableName: 'requests',
      // 'Key' defines the partition key and sort key of the item to be removed
      // - 'userId': Identity Pool identity id of the authenticated user
      // - 'requestId': path parameter
      Key: {
        PK: event.requestContext.identity.cognitoIdentityId, // PK = userId
        SK: id, // SK = requestId
      },
    };

    try {
      await call('delete', params);
      return callback(null, success({ status: true }));
    } catch (e) {
      console.log(e);
      return callback(null, failure({ status: false }));
    }
  });
}
