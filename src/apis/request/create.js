import uuid from 'uuid';
import { call } from '../../libs/dynamodb-lib';
import { success, failure } from '../../libs/response-lib';
import validate from '../../libs/validation-lib';
import { entity2Request } from '../../libs/transformation-lib';

export async function main(event, context, callback) {
  // Request body is passed in as a JSON encoded string in 'event.body'

  const data = JSON.parse(event.body);

  // - 'userId': user identities are federated through the
  //             Cognito Identity Pool, we will use the identity id
  //             as the user id of the authenticated user
  // - 'requestId': a unique uuid
  // - 'accountName': parsed from request body
  // - 'resourceType': parsed from request body
  // - 'resourceRate': parsed from request body
  // - 'quantity': parsed from request body
  // - 'submissionDate': parsed from request body
  // - 'tentativeStartDate': parsed from request body
  // - 'fulfilmentDate': parsed from request body
  // - 'status': parsed from request body
  const requestDetail = {
    userId: event.requestContext.identity.cognitoIdentityId,
    requestId: uuid.v1(),
    accountName: data.accountName,
    resourceType: data.resourceType,
    resourceRate: data.resourceRate,
    quantity: data.quantity,
    submissionDate: data.submissionDate,
    tentativeStartDate: data.tentativeStartDate,
    fulfilmentDate: data.fulfilmentDate,
    status: data.status,
  };

  // validate input
  const { errors, value } = validate(requestDetail, 'request');
  if (errors) {
    callback(null, success(Object.assign({}, { status: false, errors })));
    return;
  }

  const Item = entity2Request(value);
  // callback(null, success(Object.assign({},{ status: false, errors: {} })));
  const params = {
    TableName: 'requests',
    // 'Item' contains the attributes of the item to be created
    Item,
  };

  try {
    await call('put', params);
    callback(null, success(Object.assign({}, { item: params.Item }, { status: true })));
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false }));
  }
}
