import { call } from '../../libs/dynamodb-lib';
import { success, failure } from '../../libs/response-lib';
import validate from '../../libs/validation-lib';
import { entity2Request } from '../../libs/transformation-lib';

export async function main(event, context, callback) {
  const data = JSON.parse(event.body);

  const requestDetail = {
    userId: event.requestContext.identity.cognitoIdentityId,
    requestId: event.pathParameters.id,
    accountName: data.accountName,
    resourceType: data.resourceType,
    resourceRate: data.resourceRate,
    quantity: data.quantity,
    submissionDate: data.submissionDate,
    tentativeStartDate: data.tentativeStartDate,
    fulfilmentDate: data.fulfilmentDate,
    status: data.status,
  };

  // validate input
  const { errors, value } = validate(requestDetail, 'request');
  if (errors) {
    callback(null, success(Object.assign({}, { status: false, errors })));
    return;
  }

  const Item = entity2Request(value);

  const params = {
    TableName: 'requests',
    // 'Key' defines the partition key and sort key of the item to be updated
    // - 'userId': Identity Pool identity id of the authenticated user
    // - 'id': path parameter
    Key: {
      PK: event.requestContext.identity.cognitoIdentityId, // PK = userId
      SK: event.pathParameters.id, // SK= requestId
    },
    // 'UpdateExpression' defines the attributes to be updated
    // 'ExpressionAttributeValues' defines the value in the update expression
    UpdateExpression: `SET #GSI1PK = :accountName
                        , Attrib1 = :resourceType
                        , Attrib2 = :resourceRate
                        , Attrib3 = :quantity
                        , #LSI2SK = :submissionDate
                        , Attrib4 = :tentativeStartDate
                        , Attrib5 = :fulfilmentDate
                        , #LSI1SK = :statusDate
                        , Attrib6 = :status`,
    ExpressionAttributeNames: {
      '#GSI1PK': 'GSI1-PK',
      '#LSI2SK': 'LSI2-SK',
      '#LSI1SK': 'LSI1-SK',
    },
    ExpressionAttributeValues: {
      ':accountName': Item['GSI1-PK'],
      ':resourceType': Item.Attrib1,
      ':resourceRate': Item.Attrib2,
      ':quantity': Item.Attrib3,
      ':submissionDate': Item['LSI2-SK'],
      ':tentativeStartDate': Item.Attrib4,
      ':fulfilmentDate': Item.Attrib5,
      ':statusDate': Item['LSI1-SK'],
      ':status': Item.Attrib6,
    },
    ReturnValues: 'ALL_NEW',
  };

  try {
    await call('update', params);
    callback(null, success({ status: true }));
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false }));
  }
}
