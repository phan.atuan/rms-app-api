import uuid from 'uuid';
import { call } from '../../libs/dynamodb-lib';
import { success, failure } from '../../libs/response-lib';
import validate from '../../libs/validation-lib';
import { entity2Comment } from '../../libs/transformation-lib';

export async function main(event, context, callback) {
  // Request body is passed in as a JSON encoded string in 'event.body'

  const data = JSON.parse(event.body);

  // - 'userId': user identities are federated through the
  //             Cognito Identity Pool, we will use the identity id
  //             as the user id of the authenticated user
  // - 'requestId': parsed from request body
  // - 'commentId': a unique uuid
  // - 'text': parsed from request body
  const commentDetail = {
    userId: event.requestContext.identity.cognitoIdentityId,
    requestId: data.requestId,
    commentId: uuid.v1(),
    text: data.text,
  };

  // validate input
  const { errors, value } = validate(commentDetail, 'comment');
  if (errors) {
    callback(null, success(Object.assign({}, { status: false, errors })));
    return;
  }

  const Item = entity2Comment(value);
  // callback(null, success(Object.assign({},{ status: false, errors: {} })));
  const params = {
    TableName: 'requests',
    // 'Item' contains the attributes of the item to be created
    Item,
  };

  try {
    await call('put', params);
    callback(null, success(Object.assign({}, { item: params.Item }, { status: true })));
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false }));
  }
}
