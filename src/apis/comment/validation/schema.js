import joi from 'joi';

const comment = joi.object().keys({
  userId: joi.string().required().error(() => 'userId:User Id is required'),
  requestId: joi.string().required().error(() => 'requestId:Request Id is required'),
  commentId: joi.string().required().error(() => 'commentId:Comment Id is required'),
  text: joi.string().required().error(() => 'text:Comment is required'),
});

export default comment;
