import { call } from '../../libs/dynamodb-lib';
import { success, failure } from '../../libs/response-lib';
import { comment2entity } from '../../libs/transformation-lib';

export async function main(event, context, callback) {
  // 'KeyConditionExpression' defines the condition for the query
  // - 'PK = :requestId': only return items with matching 'requestId'
  //   partition key
  const KeyConditionExpression = 'PK = :requestId';
  const ExpressionAttributeValues = {
    ':requestId': event.pathParameters.id,
  };

  const params = {
    TableName: 'requests',
    KeyConditionExpression,
    ExpressionAttributeValues,
    ScanIndexForward: false,
  };

  const count = parseInt(event.queryStringParameters.count, 10);

  if (count > 0) {
    params.Limit = count;
  }
  try {
    const result = await call('query', params);
    // Return the matching list of items in response body
    const items = result.Items.map(comment2entity);
    callback(null, success(Object.assign({}, { items }, { status: true })));
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false }));
  }
}
