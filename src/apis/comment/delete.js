import { call } from '../../libs/dynamodb-lib';
import { success, failure } from '../../libs/response-lib';

export async function main(event, context, callback) {
  const { id } = event.pathParameters; // id = requestId
  const comments = [];

  const fetchComments = async (startKey) => {
    let queryParams = {
      TableName: 'requests',
      KeyConditionExpression: 'PK = :hkey',
      ExpressionAttributeValues: {
        ':hkey': id,
      },
      ProjectionExpression: 'PK,SK',
      // Limit: 4,
    };
    if (typeof startKey === 'object') {
      queryParams = Object.assign({}, queryParams, { ExclusiveStartKey: { PK: startKey.PK, SK: startKey.SK } });
    }
    try {
      const { Items, Count, LastEvaluatedKey } = await call('query', queryParams);
      for (let i = 0; i < Count; i++) comments.push(Items[i]);
      if (LastEvaluatedKey) {
        await fetchComments(LastEvaluatedKey);
      }
    } catch (error) {
      console.log(error);
      callback(null, failure({ status: false }));
    }
  };

  await fetchComments('START');
  const deleteComments = async (startIndex) => {
    const requests = [];
    let i;
    let batchWriteParams = {};
    for (i = startIndex; i < comments.length; i++) {
      if (i > 0 && i % 25 === 0) break;
      requests.push({
        DeleteRequest: {
          Key: {
            PK: comments[i].PK,
            SK: comments[i].SK,
          },
        },
      });
    }
    batchWriteParams = Object.assign({}, {
      RequestItems: {
        requests,
      },
    });
    await call('batchWrite', batchWriteParams);
    if (i < comments.length) await deleteComments(i);
  };

  if (comments.length > 0) {
    try {
      await deleteComments(0);
    } catch (e) {
      console.log(e);
      return callback(null, failure({ status: false }));
    }
  }

  return callback(null, success({ status: true }));
}

