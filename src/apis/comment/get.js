import { call } from '../../libs/dynamodb-lib';
import { success, failure } from '../../libs/response-lib';
import { comment2entity } from '../../libs/transformation-lib';

export async function main(event, context, callback) {
  const data = JSON.parse(event.body);
  const params = {
    TableName: 'requests',
    // 'Key' defines the partition key and sort key of the item to be retrieved
    // - 'userId': Identity Pool identity id of the authenticated user
    // - 'requestId': path parameter
    Key: {
      PK: data.requestId, // PK = requestId
      SK: data.createdAt, // SK = createdAt
    },
  };

  try {
    const result = await call('get', params);
    if (result.Item) {
      // Return the retrieved item
      callback(null, success(Object.assign(
        {},
        { item: comment2entity(result.Item) },
        { status: true },
      )));
    } else {
      callback(null, success({ status: false, errors: { message: 'Item not found.' } }));
    }
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false }));
  }
}
