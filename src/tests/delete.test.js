import { main as fnDelete } from '../apis/request/delete';
import { call } from '../libs/dynamodb-lib';
import event from '../mocks/request/delete-event.json';

jest.mock('../libs/dynamodb-lib');
jest.mock('../libs/lambda-lib');

it('delete', async () => {
  const context = 'context';
  process.env.LAMBDA_FUNC_COMMENT_DELETE = 'rms-app-api-dev-comment-delete';
  const callback = (error, response) => {
    const params = {
      TableName: 'requests',
      Key: Object.assign({}, {
        PK: event.requestContext.identity.cognitoIdentityId,
        SK: event.pathParameters.id,
      }),
    };
    const arg0 = call.mock.calls[0][0];
    const arg1 = call.mock.calls[0][1];
    expect(arg0).toBe('delete');
    expect(arg1).toEqual(params);
    expect(response.statusCode).toEqual(200);
  };

  await fnDelete(event, context, callback);
  delete process.env.LAMBDA_FUNC_COMMENT_DELETE;
});
