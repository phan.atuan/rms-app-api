import { main as fnList } from '../apis/request/list';
import { call } from '../libs/dynamodb-lib';
import event from '../mocks/request/list-event.json';

jest.mock('../libs/dynamodb-lib');

it('list', async () => {
  const context = 'context';
  const callback = (error, response) => {
    const params = {
      TableName: 'requests',
      KeyConditionExpression: 'PK = :userId',
      ExpressionAttributeValues: {
        ':userId': event.requestContext.identity.cognitoIdentityId,
      },
      ScanIndexForward: false,
      IndexName: 'LSI2',
      Limit: 3,
      ExpressionAttributeNames: null,
    };

    const arg0 = call.mock.calls[0][0];
    const arg1 = call.mock.calls[0][1];
    expect(arg0).toBe('query');
    expect(arg1).toEqual(params);
    expect(response.statusCode).toEqual(200);
  };

  await fnList(event, context, callback);
});
