import { main as fnGet } from '../apis/request/get';
import { call } from '../libs/dynamodb-lib';
import event from '../mocks/request/get-event.json';

jest.mock('../libs/dynamodb-lib');

it('get', async () => {
  const context = 'context';
  const callback = (error, response) => {
    const params = {
      TableName: 'requests',
      Key: Object.assign({}, {
        PK: event.requestContext.identity.cognitoIdentityId,
        SK: event.pathParameters.id,
      }),
    };
    const arg0 = call.mock.calls[0][0];
    const arg1 = call.mock.calls[0][1];
    expect(arg0).toBe('get');
    expect(arg1).toEqual(params);
    expect(response.statusCode).toEqual(200);
  };

  await fnGet(event, context, callback);
});
