import moment from 'moment';
import * as fnCreate from '../apis/request/create';
import * as dynamoDbLib from '../libs/dynamodb-lib';
import * as event from '../mocks/request/create-event.json';

jest.mock('../libs/dynamodb-lib');

it('create', async () => {
  const context = 'context';
  const callback = (error, response) => {
    const body = JSON.parse(event.body);
    const params = {
      TableName: 'requests',
      Item: Object.assign({}, {
        PK: event.requestContext.identity.cognitoIdentityId,
        Attrib1: body.resourceType,
        Attrib2: body.resourceRate,
        Attrib3: body.quantity,
        'GSI1-PK': body.accountName,
        'LSI2-SK': moment(body.submissionDate, 'YYYY-MM-DD').valueOf(),
        Attrib4: moment(body.tentativeStartDate, 'YYYY-MM-DD').valueOf(),
        Attrib5: null,
        Attrib6: body.status,
        'LSI1-SK': `${body.status}#${body.submissionDate}`,
      }),
    };

    const arg0 = dynamoDbLib.call.mock.calls[0][0];
    const arg1 = dynamoDbLib.call.mock.calls[0][1];
    delete arg1.Item['GSI1-SK'];
    delete arg1.Item.SK;
    expect(arg0).toBe('put');
    expect(arg1).toEqual(params);
    expect(response.statusCode).toEqual(200);
  };

  await fnCreate.main(event, context, callback);
});
