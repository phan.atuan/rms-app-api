import moment from 'moment';
import { main as fnUpdate } from '../apis/request/update';
import { call } from '../libs/dynamodb-lib';
import event from '../mocks/request/update-event.json';

jest.mock('../libs/dynamodb-lib');

it('update', async () => {
  const context = 'context';
  const callback = (error, response) => {
    const body = JSON.parse(event.body);
    const params = {
      TableName: 'requests',
      Key: {
        PK: event.requestContext.identity.cognitoIdentityId,
        SK: event.pathParameters.id,
      },
      UpdateExpression: `SET #GSI1PK = :accountName
                        , Attrib1 = :resourceType
                        , Attrib2 = :resourceRate
                        , Attrib3 = :quantity
                        , #LSI2SK = :submissionDate
                        , Attrib4 = :tentativeStartDate
                        , Attrib5 = :fulfilmentDate
                        , #LSI1SK = :statusDate
                        , Attrib6 = :status`,
      ExpressionAttributeNames: {
        '#GSI1PK': 'GSI1-PK',
        '#LSI1SK': 'LSI1-SK',
        '#LSI2SK': 'LSI2-SK',
      },
      ExpressionAttributeValues: {
        ':accountName': body.accountName,
        ':resourceType': body.resourceType,
        ':resourceRate': body.resourceRate,
        ':quantity': body.quantity,
        ':submissionDate': moment(body.submissionDate, 'YYYY-MM-DD').valueOf(),
        ':tentativeStartDate': moment(body.tentativeStartDate, 'YYYY-MM-DD').valueOf(),
        ':fulfilmentDate': moment(body.fulfilmentDate, 'YYYY-MM-DD').valueOf(),
        ':status': body.status,
        ':statusDate': `${body.status}#${body.submissionDate}`,
      },
      ReturnValues: 'ALL_NEW',
    };

    const arg0 = call.mock.calls[0][0];
    const arg1 = call.mock.calls[0][1];
    expect(arg0).toBe('update');
    expect(arg1).toEqual(params);
    expect(response.statusCode).toEqual(200);
  };

  await fnUpdate(event, context, callback);
});
