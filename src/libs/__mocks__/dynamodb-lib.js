export const call = jest.fn().mockImplementation((action) => {
  let retVal = {};
  switch (action) {
    case 'get':
      retVal = { Item: {} };
      break;
    case 'query':
      retVal = { Items: [] };
      break;
    default:
      break;
  }
  return new Promise(resolve => setImmediate(() => resolve(retVal)));
});
