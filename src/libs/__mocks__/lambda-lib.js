
export function invoke(params, cb) {
  return setImmediate(() => cb());
}
