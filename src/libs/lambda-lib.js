import AWS from 'aws-sdk';

AWS.config.update({ region: 'ap-southeast-2' });

export function invoke(params, cb) {
  const lambda = new AWS.Lambda({ region: 'ap-southeast-2' });

  return lambda.invoke(params, cb);
}
