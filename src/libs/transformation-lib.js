import moment from 'moment';

/*
  entity = {
    userId: 'USER-SUB-1234',
    requestId: 'requestId'
    accountName: 'accountName',
    resourceType: 'resourceType',
    resourceRate: N,
    quantity: M,
    submissionDate: 'YYYY-MM-DD',
    tentativeStartDate: 'YYYY-MM-DD',
    fulfilmentDate: 'YYYY-MM-DD',
    status: 'Open',
  }
*/
export function entity2Request(entity) {
  const submissionDate = (entity.submissionDate) ? moment(entity.submissionDate, 'YYYY-MM-DD').valueOf() : Date.now();
  const tentativeStartDate = (entity.tentativeStartDate) ? moment(entity.tentativeStartDate, 'YYYY-MM-DD').valueOf() : Date.now();
  const fulfilmentDate = (entity.fulfilmentDate) ? moment(entity.fulfilmentDate, 'YYYY-MM-DD').valueOf() : null;
  const request = {
    PK: entity.userId, // PK == userId
    SK: entity.requestId, // SK = requestId
    'GSI1-PK': entity.accountName, // GSI1-PK = accountName
    Attrib1: entity.resourceType, // Attrib1 = resourceType
    Attrib2: entity.resourceRate, // Attrib2 = resourceRate
    Attrib3: entity.quantity, // Attrib3 = quantity
    'LSI2-SK': submissionDate, // LSI2-SK = submissionDate
    Attrib4: tentativeStartDate, // Attrib4 = tentativeStartDate
    Attrib5: fulfilmentDate, // Attrib5 = fulfilmentDate
    Attrib6: entity.status, // Attrib6 = status
    'LSI1-SK': `${entity.status}#${moment(submissionDate).format('YYYY-MM-DD')}`, // LSI1-SK = statusDate
    'GSI1-SK': Date.now(), // GSI1-SK = createdAt
  };

  return request;
}

export function request2entity(request) {
  const entity = {
    userId: request.PK, // PK == userId
    requestId: request.SK, // SK = requestId
    accountName: request['GSI1-PK'], // GSI1-PK = accountName
    resourceType: request.Attrib1, // Attrib1 = resourceType
    resourceRate: request.Attrib2, // Attrib2 = resourceRate
    quantity: request.Attrib3, // Attrib3 = quantity
    submissionDate: request['LSI2-SK'], // LSI2-SK = submissionDate
    tentativeStartDate: request.Attrib4, // Attrib4 = tentativeStartDate
    fulfilmentDate: request.Attrib5, // Attrib5 = fulfilmentDate
    status: request.Attrib6, // Attrib6 = status
  };

  return entity;
}

/*
  entity = {
    userId: 'USER-SUB-1234',
    requestId: 'requestId'
    commentId: 'commentId',
    text: 'text',
  }
*/
export function entity2Comment(entity) {
  const createdAt = Date.now();
  const comment = {
    PK: entity.requestId, // PK == requestId
    SK: `${moment(createdAt).format('YYYY-MM-DD, HH:mm:ss.SSS')}`, // SK = createdAt 'YYYY-MM-DD, HH:mm:ss'
    'GSI1-PK': entity.commentId, // GSI1-PK = commentId
    Attrib1: entity.text, // Attrib1 = text (comment's content)
    'GSI1-SK': createdAt, // GSI1-SK = createdAt
  };

  return comment;
}

export function comment2entity(comment) {
  const entity = {
    requestId: comment.PK, // PK = requestId
    createdAt: comment.SK, // SK = createdAt
    commentId: comment['GSI1-PK'], // GSI1-PK = commentId
    text: comment.Attrib1, // Attrib1 = resourceType
  };

  return entity;
}
