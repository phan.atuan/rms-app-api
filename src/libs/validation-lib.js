import joi from 'joi';
import comment from '../apis/comment/validation/schema';
import request from '../apis/request/validation/schema';
import updateStatus from '../apis/request/validation/update-status-schema';

function parseError(error) {
  let errors = null;
  if (error) {
    errors = {};
    const regex = /\[(.*?)\]/g;
    error.message.replace(regex, (match, msg) => {
      const [key, val] = msg.split(':');
      errors[key] = val;
    });
  }
  return errors;
}
/*
const comment = joi.object().keys({
  userId: joi.string().required().error(() => 'userId:User Id is required'),
  requestId: joi.string().required().error(() => 'requestId:Request Id is required'),
  commentId: joi.string().required().error(() => 'commentId:Comment Id is required'),
  text: joi.string().required().error(() => 'text:Comment is required'),
});
*/
const schemas = {
  updateStatus,
  request,
  comment,
};

export default function validate(input, schema) {
  const { error, value } = joi.validate(input, schemas[schema], { abortEarly: false });
  const errors = parseError(error);
  return { errors, value };
}

